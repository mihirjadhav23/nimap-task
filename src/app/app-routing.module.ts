import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from '../app/category/category.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path:'', redirectTo: "home", pathMatch: "full"},
  {path:'category',component:CategoryComponent},
  {path:'home',component:HomeComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

