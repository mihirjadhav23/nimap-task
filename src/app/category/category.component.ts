import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  title = 'Category';
  all_cat : any;
  isEdit =false;
  p: number = 1;
  userObj={
    id: '',
    category_id: '',
    category_name:''
  };
  constructor(private commonService:CommonService) { }

  ngOnInit(): void {
    this.getletestcate();
  }
  addCategory(obj){
    console.log(obj);
    this.commonService.addCategory(obj).subscribe((response)=>{
      //console.log("product addeds");
      this.getletestcate();
    }) 
  }

  getletestcate(){ 
    this.commonService.getallcate().subscribe((response)=>{
      this.all_cat = response
    })
  }

  delCategory(prod){
    this.commonService.delCategory(prod).subscribe(()=>{
      this.getletestcate();
    })
  }

  editcategory(prod){
    this.isEdit=true;
    this.userObj=prod;
  }

  updateCategory(){
    this.isEdit=!this.isEdit;
    this.commonService.updateCategory(this.userObj).subscribe(()=>{
      this.getletestcate();
    }) 
  }
}
