import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { CategoryComponent } from '../category/category.component';
import { CommonService } from '../common.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { ViewChild } from '@angular/core';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],  
  template: '<h1>{{a}}</h1>'
})
export class HomeComponent implements OnInit {
  title = 'Category';
  all_prod : any;
  get_cat : any;
  isEdit =false;
  inputText :any;
 
  p: number = 1;
  userObj={
    id: '',
    product_id:'',
    product_name:'',
    category_id: '',
    category_name:''
  };
  category_name: string;
  showRate: any;
  

  constructor(private commonService:CommonService) {
  }

  ngOnInit(): void {
    this.getletestprod();
    this.getletestcate();
    
  }

  addProduct(obj){
    this.commonService.addProduct(obj).subscribe(()=>{
      //console.log("product addeds");
      this.getletestprod();
    }) 
  }

  getletestprod(){
    this.commonService.getallProduct().subscribe((response)=>{
      this.all_prod = response
    })
  }

  delProduct(prod){
    this.commonService.delProduct(prod).subscribe(()=>{
      this.getletestprod();
    })
  }

  editproduct(prod){
    this.isEdit=true;
    this.userObj=prod;
  }

  updateProduct(){
    this.isEdit=!this.isEdit;
    this.commonService.updateProduct(this.userObj).subscribe(()=>{
      this.getletestprod();
    }) 
  }
  //get category
  getletestcate(){
    this.commonService.getallcate().subscribe((response)=>{
      this.get_cat = response;
    })
  }

  onChange(){
    this.category_name = this.userObj.category_name;
    this.showRate=this.get_cat.find((o)=>o.category_name  == this.userObj.category_name);
    this.inputText=this.showRate.category_id; 
  }
}
