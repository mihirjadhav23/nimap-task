import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CategoryComponent } from './category/category.component';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
 

  constructor(private _http:HttpClient ) { }
  addProduct(product){
    console.log(product);
    return this._http.post("http://localhost:3000/product",product);
  }
  getallProduct(){
    return this._http.get("http://localhost:3000/product");
  } 
  updateProduct(product){
    console.log(product); 
    return this._http.put("http://localhost:3000/product/"+product.id, product);
  }
  delProduct(product){
    return this._http.delete("http://localhost:3000/product/"+product.id)
  }

  // category 
  addCategory(category){
    return this._http.post("http://localhost:3000/category",category);
  }
  getallcate(){
    return this._http.get("http://localhost:3000/category");
  } 
  updateCategory(category){
    console.log(category.id);
    return this._http.put("http://localhost:3000/category/"+category.id, category);
  }
  delCategory(category){
    return this._http.delete("http://localhost:3000/category/"+category.id)
  }

  //get category id and name
 
}
